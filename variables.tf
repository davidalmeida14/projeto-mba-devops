variable "project" {
  description = "Nome do projeto na cloud"
  default     = "projeto-mba"
}

variable "region" {
  description = "Definindo a região da cloud"
  default     = "us-central1"
}

variable "zone" {
  description = "Definindo a zona da cloud"
  default     = "us-central1"
}

variable "cluster_name" {
  description = "Nome do cluster"
  default     = "cluster-k8s"
}

variable "node_count" {
  description = "Quantidade de nos"
  default     = 1
}

variable "credentials" {
  description = "Credencial de acesso"
  default     = "user.json"
}

variable "external_tags" {
  description = "Tags de recurso"
  default     = ["xyz"]
}