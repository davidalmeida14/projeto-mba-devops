module "gke" {
  source        = "./modules/gke"
  credentials   = file(var.credentials)
  project       = var.project
  region        = var.region
  zone          = var.zone
  node_count    = var.node_count
  cluster_name  = var.cluster_name
  external_tags = var.external_tags
}