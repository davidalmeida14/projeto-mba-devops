terraform {
  backend "gcs" {
    bucket      = "storage-uniesp-devops-david"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}