output "endpoint" {
  value = "${module.gke.endpoint}"
}

output "instance_group_urls" {
  value = "${module.gke.instance_group_urls}"
}