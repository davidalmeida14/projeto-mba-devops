output "endpoint" {
  value = "${google_container_cluster.gke.endpoint}"
}

output "instance_group_urls" {
  value = "${google_container_cluster.gke.instance_group_urls}"
}
