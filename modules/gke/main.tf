terraform {
  required_version = "~>0.12"

  required_providers {
    google = "~> 3.6"
  }
}

locals {
  node_tags = var.external_tags
}

provider "google" {
  credentials = var.credentials
  project     = var.project
  region      = var.region
  zone        = var.zone
}

resource "google_container_cluster" "gke" {
  name                     = var.cluster_name
  initial_node_count       = var.node_count
  remove_default_node_pool = true

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    tags = local.node_tags
  }
  timeouts {
    create = "30m"
    update = "30m"
  }

}

resource "google_container_node_pool" "node_pool" {
  name               = "custom-pool"
  cluster            = google_container_cluster.gke.name
  initial_node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 4
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    machine_type = "n1-standard-1"
    disk_size_gb = 20
    disk_type    = "pd-ssd"
    preemptible  = false

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    metadata = {
      disable-legacy-endpoints = true
    }

    tags = local.node_tags
  }

  timeouts {
    create = "30m"
    update = "40m"
  }
}
